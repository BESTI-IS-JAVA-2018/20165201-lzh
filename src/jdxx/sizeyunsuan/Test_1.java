package sizeyunsuan;
import java.text.NumberFormat;
import java.util.*;
public class Test_1{
    public static void main(String[] args) {
        Biaodashi pra = new Biaodashi();
        Scanner scan = new Scanner(System.in);
        NumberFormat nf = NumberFormat.getPercentInstance();
        int Tinumber;
        String another = "y";
        String language;
        while (another.equalsIgnoreCase("y")) {
            System.out.print("\n请您选择语言~ (Select language)\n(1)简体中文\n(2)English\n(3)繁體中文" +
                    "\n输入1,2,3 (Input 1,2,3)");
            language = scan.nextLine();
            while (true) {
                if (language.equalsIgnoreCase("1")) {
                    System.out.print("\n输入您需要的题目数量：");
                    Tinumber = scan.nextInt();
                    while (true) {
                        if (Tinumber > 0) {
                            System.out.print("\n初始等级为1（两个数的运算每加一级多一个字符和一个数）\n请您输入等级：");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.CreateExpre(Tinumber, many, language);
                                    pra.showC();
                                    System.out.println("\n您答对的题目数为：" + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("您的正确率为：" + nf.format(trues));
                                    System.out.println("\n再来一次输入y重来输入n退出)");
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("输入错误！请重新输入，至少为1！");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("题目数量输入错误！请重新输入，至少为1！");
                        Tinumber = scan.nextInt();
                    }
                    break;
                } else if (language.equalsIgnoreCase("2")) {
                    System.out.print("\nPlease enter the number of questions you need: ");
                    Tinumber = scan.nextInt();
                    while (true) {
                        if (Tinumber > 0) {
                            System.out.print("\nPlease enter the level you need:");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.CreateExpre(Tinumber, many, language);
                                    pra.showE();
                                    System.out.println("\nThe number of the right answers: " + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("Accuracy is: " + nf.format(trues));
                                    System.out.println("\nTest again?input 'y' to start and 'n' to quit)");
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("The level of questions is incorrect, Please re-enter it (at least 1)");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("The number of questions is incorrect, Please re-enter it (at least 1)");
                        Tinumber = scan.nextInt();
                    }
                    break;
                } else if (language.equalsIgnoreCase("3")) {
                    System.out.print("\n請輸入您需要的題目數量");
                    Tinumber = scan.nextInt();
                    while (true){
                        if (Tinumber > 0) {
                            System.out.print("\n初始等級為1！每加一級多一個字符和一個數\n請您輸入等級:");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.CreateExpre(Tinumber, many, language);
                                    pra.showF();
                                    System.out.println("\n您答對的題目數为：" + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("正確率為：" + nf.format(trues));
                                    System.out.println("\n再來一次輸入y重來輸入n退出)");
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("輸入錯誤！請重新輸入，至少為1！");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("級別輸入錯誤！請重新輸入，至少為1");
                        Tinumber = scan.nextInt();
                    }
                    break;
                }
                else
                    System.out.println("语言选择错误！请重新输入1,2,3~");
                language = scan.nextLine();
            }
        }
    }
}
