public class Zuheshu{
    public static void main(String args[]){
        int a=Integer.parseInt(args[0]);
        int b=Integer.parseInt(args[1]);
        int sum=Digui(a,b);
        if(sum==0) {
            System.out.println("Error !!!");
        }
        else {
            System.out.println(sum);
        }
    }
    public static int Digui(int p,int q) {
        if(p<0||q<0||p<q){
            return 0;
        }
        else if(p==q||q==0){
            return 1;
        }
        else{
            return Digui(p-1,q-1)+Digui(p-1,q);
        }
    }
}
