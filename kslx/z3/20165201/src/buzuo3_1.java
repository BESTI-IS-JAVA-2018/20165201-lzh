public class buzuo3_1 {
    public static void main(String [] args) {
        int sum=0;
        for(int i=1;i<=10;i++){
            sum += fact(i);
        }
        System.out.println(sum);
    }

    public static int fact(int n) {
        if (n == 0)
            return 1;
        else
            return n * fact(n-1);
    }
}
