public class SumofRecur {
    public static void main(String [] args) {
        int a,sum=0;
        a = Integer.parseInt(args[0]);
        if(a<=0){
            System.out.println("Error!");
            return;
        }
        else if(a>=13){
            System.out.println("Number is too large!");
            return;
        }
        for(int i=1;i<=a;i++){
            sum += fact(i);
        }
        System.out.println(sum);
    }

    public static int fact(int n) {
        if (n == 0)
            return 1;
        else
            return n * fact(n-1);
    }
}
